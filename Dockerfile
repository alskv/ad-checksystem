FROM node:12 as scoreboard

RUN git clone https://github.com/HackerDom/ctf-scoreboard-client.git /repo
WORKDIR /repo/scoreboard

RUN npm install
RUN npm run build

FROM ghcr.io/hackerdom/checksystem:master

COPY --from=scoreboard /repo/scoreboard/build /scoreboard
ENV CS_STATIC=/scoreboard

ENV MOJO_CONFIG=/app/cs.conf
ENV MOJO_MODE=production

# Install checker's dependencies for current CTF.
# For example, you can use Gornilo library for simple
# way to write checkers (https://github.com/HackerDom/Gornilo)

RUN apt-get update
RUN apt-get install -y python3-pip
COPY ./private/checkers/requirements.txt /checker-requirements.txt
RUN pip install -r /checker-requirements.txt

# Copy checkers to /app/checkers catalog
COPY ./private/checkers /app/checkers
COPY logos /scoreboard/teams

# Copy config
COPY ./private/cs.private.conf /app/cs.production.conf
COPY cs.conf /app/cs.conf

EXPOSE 8080